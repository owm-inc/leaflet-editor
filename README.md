# leaflet-editor
Leaflet plugin which helps edit polygons with holes

## Example
[http://owm-inc.github.io/leaflet-editor/](http://owm-inc.github.io/leaflet-editor/)

## Install
npm install && bower install

## Run
gulp default

## Issues

Not all cases with self-intersection are solved yet.
